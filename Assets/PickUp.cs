﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

    public enum State { spawning, laying, beingPickedUp, pickedUp };

    public enum Give { nanomachines, hp, armor}

    public State state;
    public Give give;
    public float amount;
    public float speed = 0.5f;

    new Rigidbody rigidbody;
    Animator animator;
    new ParticleSystem particleSystem;

    bool terminated = false;
	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        particleSystem = GetComponent<ParticleSystem>();
        StartCoroutine(spawn());
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (state == State.beingPickedUp)
        {
            speed += 0.05f;
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, PlayerManager.Instance.charachterControl.transform.position, step);
            //Debug.Log(Vector3.Distance(transform.position, PlayerManager.Instance.charachterControl.transform.position));
            float distance = Vector2.Distance(new Vector2(transform.position.x, transform.position.z),
                new Vector2(PlayerManager.Instance.charachterControl.transform.position.x, PlayerManager.Instance.charachterControl.transform.position.z));
            //Debug.Log(step + " " + distance);
            if (distance < 0.02f)
            {
                animator.SetTrigger("Explode");
                state = State.pickedUp;
                transform.parent = PlayerManager.Instance.charachterControl.transform;
            }
        }
	}

    public IEnumerator spawn()
    {
        Vector3 prevPosition = transform.position;
        Vector3 knockdownVector = new Vector3(Random.Range(50, -50), 150, Random.Range(50, -50));

        rigidbody.AddForce(knockdownVector);

        state = State.spawning;
        //Debug.Log(gameObject.GetInstanceID() + " Spawn Exp " + Time.unscaledTime + " position: " + transform.position);
        //Debug.Log(gameObject.GetInstanceID() + " *Velocity* " + rigidbody.velocity.magnitude);
        yield return new WaitForFixedUpdate();

        while (prevPosition - transform.position != Vector3.zero)
        {
            //Debug.Log(prevPosition - transform.position);
            prevPosition = transform.position;
            yield return new WaitForFixedUpdate();
        }
        state = State.laying;
        //Debug.Log(gameObject.GetInstanceID() + " Exp Laying " + Time.unscaledTime + " position: " + transform.position);
        //Debug.Log(prevPosition - transform.position);
    }

    public void pickup()
    {
        //Debug.Log("PickUps befor deleting " + PlayerManager.Instance.pickupsAround.Count);
        state = State.beingPickedUp;
        PlayerManager.Instance.pickupsAround.Remove(this);
        //Debug.Log("PickUps after deleting " + PlayerManager.Instance.pickupsAround.Count);
    }

    void terminate()
    {
        if (!terminated)
        {
            terminated = true;
            particleSystem.Stop();
            StartCoroutine(kill());

            

            switch (give)
            {
                case Give.nanomachines:
                    GameManager.Instance.nanomachines += 1;
                    PopUpTextManager.generate(GameManager.Hero.transform.position, Color.green, "+" + 1 + " Nanomachines");
                    break;

                case Give.hp:
                    if (GameManager.Hero.hp < GameManager.Hero.hpMax)
                    {
                        PopUpTextManager.generate(GameManager.Hero.transform.position, Color.green, "+" + amount + " HP");
                        if (GameManager.Hero.hpMax - GameManager.Hero.hp > amount)
                        {
                            //GameManager.Hero.hp += amount;
                            GameManager.Hero.setHp(GameManager.Hero.hp + amount);
                        } else
                        {
                            GameManager.Hero.hp = GameManager.Hero.hpMax;
                        }
                    }
                    break;

                case Give.armor:
                    if (GameManager.Hero.armor < GameManager.Hero.armorMax)
                    {
                        if (GameManager.Hero.armorMax - GameManager.Hero.armor > amount)
                        {
                            GameManager.Hero.armor += (int)amount;
                        }
                        else
                        {
                            GameManager.Hero.armor = GameManager.Hero.armorMax;
                        }
                    }
                    break;
            }
            
        }
        
    }

    IEnumerator kill()
    {
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }

}
