﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public delegate void OnEndTakingHit();
    public OnEndTakingHit onEndTakingHit;

    public float hp = 20;
    public float hpMax = 20;
    public bool dead = false;

    CharacterControl walk;

    //Tag wich will treat as Attacking Collider and trigger hit
    public string hitTag;

    float timerTillKnockdownReset = 0;
    bool timerIsRunning = false;

    public float knockdownTrashholdTime = 2;
    public float knockdownTrashholdDamage = 6;

    float damageTaken = 0;

    Rigidbody rigbody;

    public bool invulnerable = false;

    public RectTransform hpbar;


    BlockAbility block;

    // Use this for initialization
    void Start () {
        hp = hpMax;

        rigbody = GetComponent<Rigidbody>();
        if (GetComponent<BlockAbility>() != null) block = GetComponent<BlockAbility>();

        walk = GetComponent<CharacterControl>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        GetComponent<Animator>().SetFloat("Y", transform.position.y);
        if (timerIsRunning)
        {
            timerTillKnockdownReset -= Time.deltaTime;
            if (timerTillKnockdownReset <= 0)
            {
                endKnockdownTimer();
            }
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.tag);
        if (other.tag == hitTag && !dead)
        {
            //Debug.Log("Hit right collider"); 

            if (invulnerable)
            { //do some thing to show that we didnt take damage, but could

            } else
            { //taking damage
                Debug.Log("Taking hit");
                //Checking if charachter have block
                if (block != null)
                {
                    Debug.Log("Block is presented");
                    //Check if block is active
                    if (block.block)
                    {
                        Debug.Log("Block is up");
                        //Check facing
                        //left
                        if (other.transform.position.x < transform.position.x && walk.facing == -1)
                        {
                            Debug.Log("Left" + transform.localScale.x + " " + other.transform.position.x + " " + transform.position.x);
                            GetComponent<Animator>().SetTrigger("TakingHitOnBlockTr");
                            return;
                        }

                        //right
                        else if (other.transform.position.x > transform.position.x && walk.facing == 1)
                        {
                            Debug.Log("Right " + transform.localScale.x + " " + other.transform.position.x + " " + transform.position.x);
                            GetComponent<Animator>().SetTrigger("TakingHitOnBlockTr");
                            return;
                        } else
                        {
                            block.block = false;
                        }
                    }
                }

                hp -= 2;
                if (hpbar != null && !dead)
                hpbar.localScale = new Vector3(hp/hpMax, 1 , 1);

                if (hp <= 0)
                {
                    death();
                    return;
                }

                damageTaken += 2;
                startKnockdownTimer();
                if (damageTaken >= knockdownTrashholdDamage)
                {
                    GetComponent<Animator>().SetTrigger("KnockBackTr");
                    Vector3 knockdownVector = new Vector3(0, 100, 0);

                    if (other.transform.position.x < transform.position.x)
                    {
                        knockdownVector.x = 75;
                    }
                    else
                    {
                        knockdownVector.x = -75;
                    }
                    //Debug.Log(other.transform.position.x + " " + transform.position.x + );
                    //rigbody.isKinematic = false;
                    rigbody.AddForce(knockdownVector);
                    endKnockdownTimer();

                    GetComponent<CharacterControl>().blockAttack = true;
                    GetComponent<CharacterControl>().blockMovement = true;
                } else
                {
                    GetComponent<Animator>().SetTrigger("TakingHitTr");
                }
            }
        }
    }

    void startKnockdownTimer()
    {
        timerTillKnockdownReset = knockdownTrashholdTime;
        timerIsRunning = true;
    }

    void endKnockdownTimer()
    {
        timerIsRunning = false;
        timerTillKnockdownReset = 0;
        damageTaken = 0;
    }

    void endTakingHit()
    {
        //GetComponent<Animator>().SetBool("TakingHit", false);
        //rigbody.isKinematic = true;
        if (onEndTakingHit != null)
        {
            onEndTakingHit();
        }
    }

    void endKnockdown()
    {
        GetComponent<CharacterControl>().blockAttack = false;
        GetComponent<CharacterControl>().blockMovement = false;
    }

    void death()
    {
        gameObject.layer = 12;
        dead = true;
        GetComponent<CharacterControl>().blockAttack = true;
        GetComponent<CharacterControl>().blockMovement = true;
        GetComponent<Animator>().SetTrigger("Death");
    }
}
