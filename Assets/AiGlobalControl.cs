﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AiGlobalControl : MonoBehaviour {

    public Transform player;

    //Ai who currentlu engagin player
    public List<AiEnemyControl> enemies;
    //Ai who currently attacking player
    //Enemies added here by AiEnemyControl, when they awake (change state)
    public List<AiEnemyControl> closestEnemies;

    public static AiGlobalControl overmind;

    Dictionary<characterAttackPoint, float> closestDistancesToPoints;

    public int numberOfEngagingEnemies = 2;

	// Use this for initialization
	void Awake () {
        //Debug.Log("Set overmind");
        overmind = this;
        enemies = new List<AiEnemyControl>();
        closestEnemies = new List<AiEnemyControl>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        if (enemies.Count < numberOfEngagingEnemies)
        {
            closestEnemies = enemies;
        } else
        {
            enemies.Sort();
            closestEnemies = new List<AiEnemyControl>();
            for (int i = 0; i < numberOfEngagingEnemies; i++)
            {
                closestEnemies.Add(enemies[i]);
            }
            //closestEnemies.Add(enemies[0]);
            //closestEnemies.Add(enemies[1]);
            //closestEnemies.Add(enemies[2]);
            //closestEnemies.Add(enemies[3]);
        }

        if (player.GetComponent<CharacterControl>().dead)
        {
            foreach (AiEnemyControl enemy in enemies)
            {
                if (enemy.GetComponent<AiEnemyControl>().state != AiEnemyControl.AiState.floatAround &&
                    enemy.GetComponent<AiEnemyControl>().state != AiEnemyControl.AiState.block)
                {
                    enemy.GetComponent<AiEnemyControl>().changeState(AiEnemyControl.AiState.floatAround);
                }
            }
        } else
        {
            foreach (AiEnemyControl enemy in enemies)
            {
                enemy.transform.Find("Canvas/HpBar").GetComponent<Image>().color = Color.blue;
                if (!closestEnemies.Contains(enemy) &&
                    enemy.GetComponent<AiEnemyControl>().state != AiEnemyControl.AiState.floatAround &&
                    enemy.GetComponent<AiEnemyControl>().state != AiEnemyControl.AiState.block)
                {
                    enemy.GetComponent<AiEnemyControl>().changeState(AiEnemyControl.AiState.floatAround);
                }
            }

            foreach (AiEnemyControl enemy in closestEnemies)
            {
                enemy.transform.Find("Canvas/HpBar").GetComponent<Image>().color = Color.green;

                if (enemy.GetComponent<AiEnemyControl>().state == AiEnemyControl.AiState.floatAround)
                {
                    enemy.GetComponent<AiEnemyControl>().changeState(AiEnemyControl.AiState.aproach);
                }
            }
        }

        

        //Debug.Log(closestEnemies.Count);
	}
}
