﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class actionSearcher : MonoBehaviour {
    public static actionSearcher Instance;

    public List<actionAble> itemsAround;
    actionAble closestItem;

    Transform parnt;
    new Collider collider;

	// Use this for initialization
	void Start () {
        Instance = this;
        parnt = transform.parent;
        itemsAround = new List<actionAble>();
        collider = GetComponent<Collider>();

        GameManager.onEnteringCombat += enterCombatMode;
        GameManager.onExitCombat += exitCombatMode;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (GameManager.CombatMode)
        {
            return;
        }
        

        if (itemsAround.Count > 0)
        {
            foreach (actionAble item in itemsAround)
            {
                if (closestItem == null) { closestItem = item; } else
                {
                    //If current item is closer then alredy selected
                    if (Vector3.Distance(parnt.position, closestItem.transform.position) > 
                        Vector3.Distance(parnt.position, item.transform.position))
                    {
                        closestItem = item;
                    }
                }
            }

            GameManager.actionableItem = closestItem;
        } else
        {
            GameManager.actionableItem = null;
        }
	}

    private void OnTriggerStay(Collider other)
    {
        actionAble actioanable = other.GetComponent<actionAble>();
        if (actioanable != null && !itemsAround.Contains(actioanable) && actioanable.enabled)
        {
            itemsAround.Add(actioanable);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        actionAble actioanable = other.GetComponent<actionAble>();
        if (actioanable != null &&  itemsAround.Contains(actioanable))
        {
            itemsAround.Remove(actioanable);
        }
    }

    void enterCombatMode()
    {
        collider.enabled = false;
        GameManager.actionableItem = null;
    }

    void exitCombatMode()
    {
        collider.enabled = true;
    }

}
