﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Arena))]
public class ArenaEditor : Editor {

    Arena arena;

    private void OnEnable()
    {
        arena = (Arena)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }

    private void OnSceneGUI()
    {
        GUIStyle labelStyle = new GUIStyle();
        labelStyle.fontStyle = FontStyle.Bold;
        labelStyle.fontSize = 15;
        labelStyle.wordWrap = true;
        labelStyle.normal.textColor = Color.yellow;

        Vector3 arenaPos = arena.transform.position;

        Handles.Label(arena.trigger.transform.position + new Vector3(0, 1, 0), " -Trigger- ", labelStyle);
        arena.trigger.transform.position = Handles.PositionHandle(arena.trigger.transform.position, Quaternion.identity);

        Vector3 leftBoundryPos = arena.leftBoundry.transform.position;
        Vector3[] verts = new[] {
                new Vector3(leftBoundryPos.x, leftBoundryPos.y-0.5f, leftBoundryPos.z+1),
                new Vector3(leftBoundryPos.x, leftBoundryPos.y+0.5f, leftBoundryPos.z+1),
                new Vector3(leftBoundryPos.x, leftBoundryPos.y+0.5f, leftBoundryPos.z-1),
                new Vector3(leftBoundryPos.x, leftBoundryPos.y-0.5f, leftBoundryPos.z-1)
            };
        Handles.DrawSolidRectangleWithOutline(
                verts,
                new Color(1, 0.4f, 0.4f, 0.3f),
                Color.black
            );

        Handles.Label(arena.leftBoundry.transform.position + new Vector3(0, 1, 0), " -Left Boundry- ", labelStyle);
        arena.leftBoundry.transform.position = Handles.PositionHandle(arena.leftBoundry.transform.position, Quaternion.identity);


        Vector3 rightBoundryPos = arena.rightBoundry.transform.position;
        verts = new[] {
                new Vector3(rightBoundryPos.x, rightBoundryPos.y-0.5f, rightBoundryPos.z+1),
                new Vector3(rightBoundryPos.x, rightBoundryPos.y+0.5f, rightBoundryPos.z+1),
                new Vector3(rightBoundryPos.x, rightBoundryPos.y+0.5f, rightBoundryPos.z-1),
                new Vector3(rightBoundryPos.x, rightBoundryPos.y-0.5f, rightBoundryPos.z-1)
            };
        Handles.DrawSolidRectangleWithOutline(
                verts,
                new Color(1, 0.4f, 0.4f, 0.3f),
                Color.black
            );

        Handles.Label(arena.rightBoundry.transform.position + new Vector3(0, 1, 0), " -Right Boundry- ", labelStyle);
        arena.rightBoundry.transform.position = Handles.PositionHandle(arena.rightBoundry.transform.position, Quaternion.identity);

        foreach (AiEnemyControl ai in arena.enemiesInArena)
        {
            if (ai) Handles.Label(ai.transform.position + new Vector3(0, 0.5f, 0), ai.name, labelStyle);
        }
    }

}
