﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    CharacterControl characterControl;

    public static PlayerControl Instance;

    public bool controlIsEnabled = true;

    float dashTimer = 0;
    public float dashTimerLength = 0.5f;

    //-1 - left, 1 - right
    int dashDirection = 0;

	// Use this for initialization
	void Awake () {
        Instance = this;
        characterControl = GetComponent<CharacterControl>();
	}
	
    

	void FixedUpdate () {
        if (characterControl.dead || !controlIsEnabled) return;
        if (DialogueManager.Instance.IsDialogInProgress()) return;
        characterControl.moveX = Input.GetAxisRaw("Horizontal");
        characterControl.moveY = Input.GetAxisRaw("Vertical");

        /*if (Input.GetAxisRaw("Sprint") > 0)
        {
            characterControl.sprint = true;
        } else
        {
            characterControl.sprint = false;
        }*/

        

        if (dashTimer > 0) dashTimer -= Time.fixedDeltaTime;

        if (GameManager.CombatMode)
        {
            if (Input.GetButtonDown("Kick1"))
            {
                characterControl.kick();
            }

            if (Input.GetButton("Punch1"))
            {
                characterControl.punch();
            }

            if (Input.GetButton("Right") && Input.GetButton("Sprint"))
            {
                dash(1);
            }

            if (Input.GetButton("Left") && Input.GetButton("Sprint"))
            {
                dash(-1);
            }
        } else
        {
            if (Input.GetButtonUp("Use")/* || Input.GetButtonUp("Punch1")*/)
            {
                if (GameManager.actionableItem != null && DialogueManager.Instance.IsDialogInProgress() == false)
                {
                    GameManager.actionableItem.engageInAction();
                }
            }
        }
    }

    void dash(int direction)
    {
        if (dashTimer > 0)
        {
            if (direction == dashDirection)
            {
                //preform dash
                StartCoroutine(characterControl.dashing(direction));
                dashDirection = 0;
                dashTimer = 0;
            } else
            {
                //change direction of dash
                dashDirection = direction;
                dashTimer = dashTimerLength;
            }
        } else
        { //ini dash
            dashDirection = direction;
            dashTimer = dashTimerLength;
        }
    }
}
