﻿using AdvancedInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class Arena : MonoBehaviour {
    public delegate void OnArenaClear();
    public OnArenaClear onArenaClear;

    [Inspect]
    public List<AiEnemyControl> enemiesInArena;

    [Inspect]
    public Transform leftBoundry;
    [Inspect]
    public Transform rightBoundry;
    [Inspect]
    public Collider trigger;

    [Inspect]
    public bool dialog;

    public DialogLuncher dialogLuncher;

    public bool cleared = false;

    //If true - trigger wont engage
    public bool isSafe = false;
	// Use this for initialization
	void Start () {
        if (dialog) dialogLuncher = GetComponent<DialogLuncher>();

    }
	
	void FixedUpdate () {
        if (cleared) return;

		if (!enemiesAlive())
        {
            disengageArena();
        }
	}

    //Return true if at least one enemy is still alive in the list
    bool enemiesAlive()
    {
        foreach (AiEnemyControl ai in enemiesInArena)
        {
            if (!ai.GetComponent<CharacterControl>().dead)
            {
                return true;
            }
        }
        return false;
    }

    

    public void engageArena()
    {
        GameManager.enterCombatMode();
        leftBoundry.gameObject.SetActive(true);
        rightBoundry.gameObject.SetActive(true);
        foreach (AiEnemyControl ai in enemiesInArena)
        {
            ai.changeState(AiEnemyControl.AiState.idle);
        }
        trigger.gameObject.SetActive(false);
    }

    void disengageArena()
    {
        StartCoroutine(GameManager.exitCombatMode());
        leftBoundry.gameObject.SetActive(false);
        rightBoundry.gameObject.SetActive(false);
        cleared = true;
        if (onArenaClear != null) onArenaClear();
    }

    public void playerEnterTrigger()
    {
        if (isSafe) return;

        if (dialog)
        {
            //DialogueManager.Instance.StartDialogue(dialog);
            dialogLuncher.lunchDialog();
            
        } else
        {
            engageArena();
        }
    }

    public void rearmArena(List<AiEnemyControl> enemies, TextAsset dialog = null, bool safe = false)
    {
        this.dialog = dialog;
        isSafe = safe;
        enemiesInArena = new List<AiEnemyControl>(enemies);
        cleared = false;

        leftBoundry.gameObject.SetActive(false);
        rightBoundry.gameObject.SetActive(false);

        trigger.gameObject.SetActive(true);
    }
}
