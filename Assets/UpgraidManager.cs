﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdvancedInspector;
using UnityEngine.UI;
using System;

[AdvancedInspector]
public class UpgraidManager : MonoBehaviour {

    public static UpgraidManager Instance;

    [Inspect]
    public Canvas canvas;

    [Inspect]
    public CharacterControl hero;

    [Inspect(0)]
    public GridLayoutGroup hpUpgraidGrid;
    [Inspect(0)]
    public GridLayoutGroup attackUpgraidGrid;
    [Inspect(0)]
    public GridLayoutGroup armourUpgraidGrid;

    [Inspect(1), Constructor("InvokeConstructor")]
    public Upgraid hpUpgraid;
    [Inspect(1), Constructor("InvokeConstructor")]
    public Upgraid attackUpgraid;
    [Inspect(1), Constructor("InvokeConstructor")]
    public Upgraid armourUpgraid;

    public Upgraid InvokeConstructor()
    {
        return new Upgraid();
    }

    // Use this for initialization
    void Start () {

        Instance = this;
    }

    public static void showScreen()
    {
        Instance.canvas.gameObject.SetActive(true);
        Instance.renderScreen(); 
    }

    public void hideScreen()
    {
        Instance.canvas.gameObject.SetActive(false);
    }

    public void renderScreen()
    {

        renderUpgraid(hpUpgraid);
        renderUpgraid(attackUpgraid);
        renderUpgraid(armourUpgraid);

        hpUpgraid.onUpgraid += onHpUpgraid;
        attackUpgraid.onUpgraid += onAttackUpgraid;
        armourUpgraid.onUpgraid += onArmorUpgraid;
    }

    public void renderUpgraid(Upgraid upgraid)
    {
        foreach (Transform child in upgraid.grid.transform)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < upgraid.currentLevel; i++)
        {
            Image boughtImg = Instantiate(upgraid.boughtSkill.gameObject, upgraid.grid.transform).GetComponent<Image>();
            boughtImg.sprite = upgraid.boughtSprite;
        }
        if (upgraid.currentLevel < upgraid.maxLevel)
        {
            Button btn = Instantiate(upgraid.buySkill.gameObject, upgraid.grid.transform).GetComponent<Button>();
            btn.GetComponent<Image>().sprite = upgraid.buySprite;
            btn.transform.Find("Text").GetComponent<Text>().text = (upgraid.currentLevel+1) * 25 +"";
            upgraid.ini(btn);
        }
        if (upgraid.maxLevel - upgraid.currentLevel > 1)
        {
            //for (int i = 0; i < upgraid.maxLevel - upgraid.currentLevel - 1; i++)
            for (int i = upgraid.currentLevel+1; i < upgraid.maxLevel; i++)
            {
                GameObject btn = Instantiate(upgraid.unavailableSkill.gameObject, upgraid.grid.transform);
                btn.transform.
                    Find("Text").
                    GetComponent<Text>().
                    text = (1 + i) * 25 + "";
                btn.transform.Find("Text").GetComponent<Text>().color = Color.gray;
            }
        }
    }

    public void onHpUpgraid()
    {
        hero.hpMax += 5;
        renderUpgraid(hpUpgraid);
    }

    public void onAttackUpgraid()
    {
        hero.damagePunch += 1;
        renderUpgraid(attackUpgraid);
    }

    public void onArmorUpgraid()
    {
        hero.armor += 1;
        hero.armorMax += 1;
        hero.armorRegenMax = 1;
        renderUpgraid(armourUpgraid);
    }
}

[Serializable]
public class Upgraid
{
    public delegate void OnUpgraid();
    public OnUpgraid onUpgraid;

    public int maxLevel = 4;
    public int currentLevel = 0;

    public GridLayoutGroup grid;
    public Image boughtSkill;
    public Button buySkill;
    public Image unavailableSkill;

    public Sprite buySprite;
    public Sprite boughtSprite;

    public void ini(Button btn)
    {
        btn.onClick.AddListener(upgraid);
    }

    public void upgraid()
    {
        Debug.Log(25 * (currentLevel + 1));
        if (currentLevel < maxLevel && GameManager.Instance.nanomachines >= 25 * (currentLevel + 1))
        {
            GameManager.Instance.nanomachines -= 25 * (currentLevel + 1);
            currentLevel++;
            if (onUpgraid != null)
            {
                onUpgraid();
            }
        }
    }
}

