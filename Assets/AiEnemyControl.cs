﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdvancedInspector;

[AdvancedInspector]
public class AiEnemyControl : MonoBehaviour, IComparable<AiEnemyControl>
{

    CharacterControl characterControl;

    AiGlobalControl overmind;

    public Transform player;
    public CharacterControl playerCharachterControl;

    public enum AiState {sleep, idle, aproach, moveTo, floatAround, prepToAttack, attack, block, takeHit};
    public AiState state;

    //Wher AI will walk
    Vector3 aproachPoint;

    public delegate void OnFinishMoveTo();
    public OnFinishMoveTo onFinishMoveTo;

    //At what distance Ai will engage with player
    [Inspect]
    public float DistanceToEngageWithPlayer = 2;

    //How close Ai need to aproach for punch
    [Inspect]
    public float distanceForAttackX = 0.32f;
    [Inspect]
    public float distanceForAttackY = 0.015f;

    //Time, untill Ai will hit player when he aproach him
    [Inspect]
    public float attackDelay = 1;
    //Actuall timer for delay befor hit
    float attackDelayTime = 0;

    //How long cop will stay in block mode
    [Inspect]
    public float blockTimeLength = 3;
    float blockTime = 0;
    //Wich state charachter will take after block
    AiState stateAfterBlock = AiState.prepToAttack;

    //Value for hovering around
    float borderYTop = 0.85f;
    float borderYBottom = -0.75f;
    float borderX = 1.4f;

    //float closer borders
    float tooCloseX = 0.9f;
    float tooCloseY = 0.8f;

    //Wher character will walk, while hovering around
    Vector3 hoveringTarget;

    //If true - charachter will stay at place for a while
    bool hoverWait = true;

    //How long charachter will wait
    float hoverWaitColdown = 1;
    float hoverWaitColdownLength = 3;

    [Inspect]
    public List<GameObject> loot;
    void Start () {
        player = GameManager.Instance.hero.transform;
        playerCharachterControl = player.GetComponent<CharacterControl>();

        //Debug.Log("Set ai controller");
        overmind = AiGlobalControl.overmind;

        characterControl = GetComponent<CharacterControl>();
        
        characterControl.onEndAttack += finishAttack;

        characterControl.onEndTakingHit += onTakingHit;
        characterControl.onEndTakingHit += enterBlock;

        characterControl.onDeath += onDeath;

        characterControl.target = player;
    }

	void FixedUpdate () {
        //Lets make sure player is still up
        if (characterControl.dead)
        {
            return;
        }

        float distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);
        float xDifference = Mathf.Abs(player.transform.position.x - transform.position.x);
        float yDifference = Mathf.Abs(player.transform.position.z - transform.position.z);

        switch (state)
        {
            case AiState.sleep:
                
                //SLeep is disengaged mode, when Ai only check distance to player and he is not in Ai enem,ies control group
                /*if (xDifference < DistanceToEngageWithPlayer) //Player close enough to start aproaching him
                {
                    changeState(AiState.aproach);
                    aproachPoint = player.transform.position;
                }*/
                break;

            case AiState.idle:

                //Idle is state for Ai, from witch he decide how to engage player
                if (playerInRangeForAttack()) //Player so close we can attack him alredy
                {
                    attackDelayTime = attackDelay;
                    changeState(AiState.prepToAttack);
                } else //if (xDifference < DistanceToEngageWithPlayer) //Player close enough to start aproaching him
                {
                    changeState(AiState.aproach);
                    aproachPoint = player.transform.position;
                } /*else if (xDifference > DistanceToEngageWithPlayer*1.5f)
                {
                    changeState(AiState.sleep);
                }*/

            break;

            case AiState.aproach:
                aproachPoint = playerCharachterControl.getClosestAproachPoint(transform.position);
                //if (playerInRangeForAttack()) //If player close enough - start attack
                if (Vector3.Distance(aproachPoint, transform.position) <= 0.06f)
                {
                    attackDelayTime = attackDelay;         //Delay attack abit
                    changeState(AiState.prepToAttack);     //Start attack
                }
                else //if(distanceToPlayer <= DistanceToEngageWithPlayer) //If player still in reach - walk towards it
                {
                    //walk horizontaly
                    //if (xDifference > distanceForAttackX)
                    if (Mathf.Abs(aproachPoint.x - transform.position.x) > 0.03f)
                    {
                        if (aproachPoint.x < transform.position.x)
                            characterControl.moveX = -1;
                        else
                            characterControl.moveX = 1;
                    }
                    else
                    {
                        characterControl.moveX = 0;
                    }

                    //walk vertically
                    //if (yDifference > distanceForAttackY)
                    if (Mathf.Abs(aproachPoint.z - transform.position.z) > 0.03f)
                    {
                        if (aproachPoint.z < transform.position.z)
                            characterControl.moveY = -1;
                        else
                            characterControl.moveY = 1;
                    }
                    else
                    {
                        characterControl.moveY = 0;
                    }
                }
                break;

            case AiState.moveTo:

                if (Vector3.Distance(transform.position, aproachPoint) < 0.2) //Ai arrived to aproach point
                {
                    changeState(AiState.sleep);
                    if (onFinishMoveTo != null)
                    {
                        onFinishMoveTo();
                        onFinishMoveTo = null;
                    }
                }
                else //We keep going to point
                {
                    //walk horizontaly
                    if (Mathf.Abs(aproachPoint.x - transform.position.x) > 0.1)
                    {
                        if (aproachPoint.x < transform.position.x)
                            characterControl.moveX = -1;
                        else
                            characterControl.moveX = 1;
                    }
                    else
                    {
                        characterControl.moveX = 0;
                    }

                    //walk vertically
                    if (Mathf.Abs(aproachPoint.z - transform.position.z) > 0.1)
                    {
                        if (aproachPoint.z < transform.position.z)
                            characterControl.moveY = -1;
                        else
                            characterControl.moveY = 1;
                    }
                    else
                    {
                        characterControl.moveY = 0;
                    }
                }
                break;

            case AiState.prepToAttack:

                attackDelayTime -= Time.fixedDeltaTime;
                if (!checkFacingToTarget(player) && attackDelayTime > attackDelay/2)   //While prepare to attack, switch facing, if no longer facing player
                    flip();
                if (attackDelayTime <= 0)
                {
                    characterControl.kick();
                    changeState(AiState.attack);
                }

                break;

            case AiState.attack:

                //not much to do here...

                break;

            case AiState.takeHit:



                break;

            case AiState.block:

                blockTime -= Time.deltaTime;
                if (blockTime <= 0) //Time to leave block
                {
                    GetComponent<BlockAbility>().removeBlock();
                    changeState(stateAfterBlock);
                }

                break;

            case AiState.floatAround:
                if (hoverWait)
                {
                    hoverWaitColdown -= Time.fixedDeltaTime;
                    if (hoverWaitColdown <= 0) //finish waiting, time to go to new point
                    {
                        
                        hoverWait = false;
                        hoverWaitColdown = 0;
                        hoveringTarget = getNewPointForHover();
                        //Debug.Log("Done waiting, getting new point to move to - " + hoveringTarget);
                    }
                } else /*if (hoveringTarget != null)*/
                {
                    if ((Mathf.Abs(transform.position.x - hoveringTarget.x) <= 0.07) && (Mathf.Abs(transform.position.z - hoveringTarget.z) <= 0.07))//(Vector3.Distance(transform.position, hoveringTarget) <= 0.09) //We reach our target and can chill a little
                    {
                        //Debug.Log("Arrived to target location - " + hoveringTarget);
                        hoverWait = true;
                        hoverWaitColdown = hoverWaitColdownLength;
                        characterControl.moveX = 0;
                        characterControl.moveY = 0;
                    } else //we still need to walk towards target
                    { 
                        //walk horizontaly
                        if (Mathf.Abs(transform.position.x - hoveringTarget.x) > 0.07)
                        {
                            if (transform.position.x > hoveringTarget.x)
                                characterControl.moveX = -1;
                            else
                                characterControl.moveX = 1;
                        }
                        else
                        {
                            characterControl.moveX = 0;
                        }

                        //walk vertically
                        if (Mathf.Abs(transform.position.z - hoveringTarget.z) > 0.07)
                        {
                            if (hoveringTarget.z < transform.position.z)
                                characterControl.moveY = -1;
                            else
                                characterControl.moveY = 1;
                        }
                        else
                        {
                            characterControl.moveY = 0;
                        }
                    }
                }

                break;
        }
	}

    Vector3 getNewPointForHover()
    {
        Vector3 result = new Vector3(0,0,0);
        float playerX = player.transform.position.x;
        float playerY = player.transform.position.z;
        float resultX = UnityEngine.Random.Range(playerX - borderX, playerX + borderX);
        float resultY;
        if (resultX > playerX - tooCloseX && resultX < playerX + tooCloseX) //We are too cose to player, need to stay in outer radiuse
        {
            if (playerY + tooCloseY  > borderYTop)
            {
                resultY = UnityEngine.Random.Range(playerY - tooCloseY, borderYBottom);
            } else if (playerY - tooCloseY < borderYBottom)
            {
                resultY = UnityEngine.Random.Range(playerY + tooCloseY, borderYTop);
            } else
            {
                if (UnityEngine.Random.Range(-1, 1) >= 0)
                {
                    resultY = UnityEngine.Random.Range(playerY - tooCloseY, borderYBottom);
                } else
                {
                    resultY = UnityEngine.Random.Range(playerY + tooCloseY, borderYTop);
                }
            }
        } else
        {
            resultY = UnityEngine.Random.Range(borderYTop, borderYBottom);
        }

        result.x = resultX;
        result.z = resultY;

        //Debug.Log(result);

        return result;
    }

    bool playerInRangeForAttack()
    {
        float xDifference =Mathf.Abs(Mathf.Abs(player.transform.position.x) - Mathf.Abs(transform.position.x));
        float yDifference = Mathf.Abs(Mathf.Abs(player.transform.position.z) - Mathf.Abs(transform.position.z));

        if (xDifference < distanceForAttackX &&
            yDifference < distanceForAttackY)
        {
            return true;
        } else
        {
            return false;
        }
    }

    //After ending attack reset state
    void finishAttack()
    {
        changeState(AiState.idle);
    }

    void enterBlock()
    {
        if (GetComponent<BlockAbility>().blockAvailable)
        {
            blockTime = blockTimeLength;
            GetComponent<BlockAbility>().blockUp();
            changeState(AiState.block);
        }
    }

    void flip()
    {
        characterControl.flip();
    }

    bool checkFacingToTarget(Transform target)
    {
        //facing left - target on the left
        if (characterControl.transform.localScale == characterControl.lookLeft && transform.position.x > target.transform.position.x)
        {
            return true;
        }//facing right - target on right
        else if (characterControl.transform.localScale == characterControl.lookRight && transform.position.x < target.transform.position.x)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public void changeState(AiState state)
    {
        //Debug.Log("Changing state from " + this.state + " to " + state);
        //waking up
        if (this.state == AiState.sleep && state != AiState.sleep)
        {
            overmind.enemies.Add(this);
        }
        else if (this.state != AiState.floatAround && state == AiState.floatAround) //Entering hover mode
        {
            characterControl.hoverAround = true;
        }
        else if (this.state == AiState.floatAround && state != AiState.floatAround) //Exiting hover mode
        {
            characterControl.hoverAround = false;
            hoverWaitColdown = 1;
            hoverWait = true;
        } else if (GetComponent<BlockAbility>().block && this.state == AiState.block)
        {
            stateAfterBlock = state;
            return;
        }

        this.state = state;

        //eneter sleep mode
        if (state == AiState.sleep)
        {
            //Debug.Log("Removing enemy from array");
            if (overmind.
                enemies.
                Contains(this)) overmind.enemies.Remove(this);
        }
    }

    public void changeState(AiState state, Vector3 point)
    {
        if (state == AiState.moveTo)
        {
            aproachPoint = point;
            this.state = state;
        } else
        {
            Debug.LogError("Missused changeState, only send Vector3 for moveTo state.");
        }
    }

    void onDeath()
    {
        if (overmind.enemies.Contains(this)) overmind.enemies.Remove(this);

        foreach (GameObject PickUp in loot)
        {
            Instantiate(PickUp, transform.position, Quaternion.identity);
        }
    }

    void onTakingHit()
    {
        if (state == AiState.prepToAttack) attackDelayTime = attackDelay;
    }

    public int CompareTo(AiEnemyControl other)
    {
        float distance1 = Vector3.Distance(transform.position, this.player.transform.position);
        float distance2 = Vector3.Distance(other.transform.position, other.player.transform.position);

        if (distance1 > distance2)
        {
            return 1;
        } else if (distance1 == distance2)
        {
            return 0;
        }
            else
        {
            return -1;
        }
    }
}
