﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockAbility : MonoBehaviour {


    Animator animator;

    CharacterControl health;

    public bool block = false;
    public bool blockAvailable = true;

    float blockColdown = 0;
    public float blockColdownLength = 6;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        health = GetComponent<CharacterControl>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (blockColdown > 0)
        {
            blockColdown -= Time.fixedDeltaTime;
            if (blockColdown <= 0)
            {
                blockAvailable = true;
                blockColdown = 0;
            }
        }

    }

    public void blockUp()
    {
        animator.SetTrigger("EnterBlock");
        blockColdown = blockColdownLength;
        blockAvailable = false;
        block = true;
    }

    public void removeBlock()
    {
        animator.SetTrigger("LeaveBlock");
        block = false;
    }
}
