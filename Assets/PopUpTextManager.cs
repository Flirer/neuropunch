﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpTextManager : MonoBehaviour {

    public static PopUpTextManager Instance;
    public GameObject prefab;
    public Canvas canvas;
    public Camera cam;

    // Use this for initialization
    void Awake () {
        Instance = this;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void generate(Vector3 position, Color color, string text)
    {
        GameObject textObj = Instantiate(
            Instance.prefab);
        textObj.transform.SetParent(Instance.canvas.transform, false);
        textObj.GetComponent<PopUpText>().ini(position, color, text, Instance.cam);
    }
}
