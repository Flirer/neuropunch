﻿using AdvancedInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AdvancedInspector]
public class DialogLuncher : MonoBehaviour {

    [Inspect]
    [SerializeField]
    public TextAsset dialog;
    [Inspect]
    [SerializeField]
    public Arena arena;
    [Serializable]
    public class CharachtersInDialog : UDictionary<string, GameObject> { }
    [Inspect]
    public CharachtersInDialog charachtersInDialog = new CharachtersInDialog();


    // Use this for initialization
    void Awake () {
        CharacterControl charachter = GetComponent<CharacterControl>();
		if (charachtersInDialog.Count == 0 && charachter != null)
        {
            charachtersInDialog.Add("_default", charachter.gameObject);
        }
	}
	
	public void lunchDialog()
    {
            if (arena != null) DialogueManager.currentlyRelatedArena = arena;

            DialogueManager.Instance.charachtersInDialog = new Dictionary<string, GameObject>();
            foreach (KeyValuePair<string, GameObject> charachter in charachtersInDialog)
            {
                DialogueManager.Instance.charachtersInDialog.Add(charachter.Key, charachter.Value);
            }

            DialogueManager.Instance.StartDialogue(dialog);
    }
}
