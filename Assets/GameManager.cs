﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdvancedInspector;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public delegate void OnEnteringCombat();
    public static OnEnteringCombat onEnteringCombat;

    public delegate void OnExitCombat();
    public static OnExitCombat onExitCombat;

    [Inspect]
    public List<Arena> arenas;
    [Inspect]
    public CharacterControl hero;

    public static GameManager Instance;
    public static bool CombatMode = false;

    //Item currently highlighted
    public static actionAble actionableItem;
    public static CharacterControl Hero;

    public Image deathScreen;
    public Image victoryScreen;

    public float nanomachines {
        get { return _nanomachines; }
        set
        {
            _nanomachines = value;
            nanomachineLabel.text = _nanomachines.ToString();
        }
    }


    float _nanomachines = 0;

    [Inspect]
    public Text nanomachineLabel;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        Hero = hero;
        nanomachines += 25;
        hero.onDeath += iniGameOver;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static void engageArena(int arena)
    {
        if (arena == -1)
        {
            DialogueManager.currentlyRelatedArena.engageArena();
        }
        else if (arena < Instance.arenas.Count)
        {
            Instance.arenas[arena].engageArena();
        }
    }

    public static void makeArenaSafe(int arena)
    {
        if (arena < Instance.arenas.Count)
        {
            Instance.arenas[arena].isSafe = true;
        }
    }

    public static void enterCombatMode()
    {
        Debug.Log("Enterting combat mode...");
        CombatMode = true;
        if (onEnteringCombat != null)
        {
            onEnteringCombat();
        }
    }

    public static IEnumerator exitCombatMode()
    {
        Debug.Log("Exiting combat mode...");
        yield return new WaitForSeconds(1);
        CombatMode = false;
        if (onExitCombat != null)
        {
            onExitCombat();
        }
        actionableItem = null;
    }

    public void iniGameOver()
    {
        StartCoroutine(gameOver());
    }

    public IEnumerator gameOver()
    {
        yield return new WaitForSeconds(2);
        while (Instance.deathScreen.color.a < 1)
        {
            yield return null;
            Color clr = Instance.deathScreen.color;
            clr.a += 0.025f;
            Instance.deathScreen.color = clr;
        }
        yield return new WaitForSeconds(4);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void iniVictory()
    {
        StartCoroutine(victory());
    }

    public IEnumerator victory()
    {
        yield return new WaitForSeconds(2);
        while (Instance.victoryScreen.color.a < 1)
        {
            yield return null;
            Color clr = Instance.victoryScreen.color;
            clr.a += 0.025f;
            Instance.victoryScreen.color = clr;
        }
    }

}