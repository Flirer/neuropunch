﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager Instance;

    public int framesBeforNextPickUp = 3;
    int framesBeforNextPickUpLeft = 0;

    public List<PickUp> pickupsAround;

    public CharacterControl charachterControl;

    new Collider collider;

    // Use this for initialization
    void Start () {
        Instance = this;
        pickupsAround = new List<PickUp>();
        collider = GetComponent<Collider>();


        GameManager.onEnteringCombat += enterCombatMode;
        GameManager.onExitCombat += exitCombatMode;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (GameManager.CombatMode)
        {
            return;
        }

        if (framesBeforNextPickUpLeft > 0)
        {
            framesBeforNextPickUpLeft--;
            return;
        }
        if (pickupsAround.Count > 0)
        {
            PickUp closestPickUp = pickupsAround[0];
            foreach (PickUp pickup in pickupsAround)
            {
                if (Vector3.Distance(charachterControl.transform.position, pickup.transform.position) <
                    Vector3.Distance(charachterControl.transform.position, closestPickUp.transform.position))
                    closestPickUp = pickup;
            }
            framesBeforNextPickUpLeft = framesBeforNextPickUp;
            closestPickUp.pickup();
        }
	}

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "PickUp" &&
            !pickupsAround.Contains(other.GetComponent<PickUp>()) &&
            other.GetComponent<PickUp>().state == PickUp.State.laying)
        {
            pickupsAround.Add(other.GetComponent<PickUp>());
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "PickUp" && 
            pickupsAround.Contains(other.GetComponent<PickUp>()) &&
            other.GetComponent<PickUp>().state == PickUp.State.laying) pickupsAround.Remove(other.GetComponent<PickUp>());
    }

    void enterCombatMode()
    {
        collider.enabled = false;
    }

    void exitCombatMode()
    {
        collider.enabled = true;
    }

}
