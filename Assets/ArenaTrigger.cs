﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaTrigger : MonoBehaviour {
    public Arena arena;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerHitBox")
        {
            //Debug.Log("Player enter trugger");
            arena.playerEnterTrigger();
        }
    }
}
