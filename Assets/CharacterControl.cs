﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AdvancedInspector;

[AdvancedInspector]
public class CharacterControl : MonoBehaviour {

    public delegate void EndAttack();
    public EndAttack onEndAttack;

    public delegate void EndDash();
    public EndDash onEndDash;

    public delegate void OnEndTakingHit();
    public OnEndTakingHit onEndTakingHit;

    public delegate void OnDeatht();
    public OnEndTakingHit onDeath;

    public delegate void OnEnterCombat();
    public OnEnterCombat onEnterCombat;

    public delegate void OnExitCombat();
    public OnExitCombat onExitCombat;

    Animator animator;
    BlockAbility block;

    public List<characterAttackPoint> attackPoints;

    //Tag wich will treat as Attacking Collider and trigger hit
    [Inspect]
    public string hitTag;

    public float hp {
        get
        {
            return _hp;
        }
        set
        {
            _hp = value;
            if (hpbar != null && !dead)
                hpbar.localScale = new Vector3(_hp / hpMax, 1, 1);

            if (_hp <= 0)
            {
                death();
                return;
            }
            
        }
    }

    public float _hp = 20;
    [Inspect]
    public float hpMax = 20;

    public int armor {
        get
        {
            return _armor;
        }
        set
        {
            if (value > _armor)
            {
                PopUpTextManager.generate(transform.position, Color.green, "+" + (value - _armor) + " Armor");
            }
            else
            {
                PopUpTextManager.generate(transform.position, Color.red, "-" + (_armor - value) + " Armor");
            }
            _armor = value;
            for (int i = 0; i < 4; i++)
            {
                if (armor > i)
                {
                    armorbar.GetChild(i).gameObject.SetActive(true);
                }
                else
                {
                    armorbar.GetChild(i).gameObject.SetActive(false);
                }
            }
            
        }
    }
    public int _armor = 0;
    [Inspect]
    public int armorMax = 0;
    [Inspect]
    public int armorRegenMax {
        set
        {

        }
        get
        {
            return armorMax;
        }
    }
    [Inspect]
    public float armorRegenTime = 4;
    public float armorRegenTimeLeft = 4;

    public bool dead = false;

    //Character move speed
    [Inspect]
    public float moveSpeed = 2;
    float moveSpeedActual;

    //Block events from triggering
    public bool blockMovement = false;
    public bool blockAttack = false;

    //Input from devices or Ai
    public float moveX = 0;
    public float moveY = 0;


    public bool sprint = false;

    public Vector3 lookRight = new Vector3(1, 1, 1);
    public Vector3 lookLeft = new Vector3(-1, 1, 1);

    //dash coldown timer
    float dashColdown = 0;
    [Inspect]
    public float dashColdownLength = 1.5f;

    //-1 left, 1 right
    public int facing = 1;

    [Inspect]
    public RectTransform hpbar;

    [Inspect]
    public RectTransform armorbar;

    float timerTillKnockdownReset = 0;

    //How long time should pass after hit to reset trashhol damage
    [Inspect]
    public float knockdownTrashholdTime = 2;
    //How many damage character should get, befor he will be knockdowned
    [Inspect]
    public float knockdownTrashholdDamage = 6;

    float damageTaken = 0;

    Rigidbody rigbody;

    public bool invulnerable = false;

    public bool hoverAround = false;

    public Transform target;

    [Inspect]
    public float damagePunch = 2;
    [Inspect]
    public float damageKick = 3;


    public float damageCurrent = 0;

    int defaultLayer;

    new Rigidbody rigidbody;

    // Use this for initialization
    void Start () {
        moveSpeedActual = moveSpeed;
        animator = GetComponent<Animator>();

        defaultLayer = gameObject.layer;
        //onEndTakingHit += resetPunch;

        hp = hpMax;

        rigbody = GetComponent<Rigidbody>();
        if (GetComponent<BlockAbility>() != null) block = GetComponent<BlockAbility>();

        attackPoints = new List<characterAttackPoint>();
        attackPoints.Add(new characterAttackPoint(new Vector3(0.3f, 0, 0.1f)));
        attackPoints.Add(new characterAttackPoint(new Vector3(0.3f, 0, -0.1f)));
        attackPoints.Add(new characterAttackPoint(new Vector3(-0.3f, 0, 0.1f)));
        attackPoints.Add(new characterAttackPoint(new Vector3(-0.3f, 0, -0.1f)));

        
        GameManager.onEnteringCombat += enterCombat;
        GameManager.onExitCombat += exitCombat;

        rigidbody = GetComponent<Rigidbody>();


    }

    public void setHp(float value)
    {
        if (value > hp)
        {
            PopUpTextManager.generate(transform.position, Color.green, "+" + (value - hp) + " HP");
        }
        else
        {
            PopUpTextManager.generate(transform.position, Color.red, "-" + (hp - value) + " HP");
        }
        hp = value;
    }

    void enterCombat()
    {
        if (GetComponent<AiEnemyControl>())
        {
            hpbar.parent.gameObject.SetActive(true);
        }
        if (GetComponent<PlayerControl>())
        {
            animator.SetBool("inCombat", true);
        }
        if (onEnterCombat != null) onEnterCombat();
    }

    void exitCombat()
    {
        if (GetComponent<AiEnemyControl>())
        {
            hpbar.parent.gameObject.SetActive(false);
        }
        if (GetComponent<PlayerControl>())
        {
            animator.SetBool("inCombat", false);
        }
        if (onExitCombat != null) onExitCombat();
    }

    
    void FixedUpdate()
    {
        if (hoverAround)
        {
            moveSpeedActual = 0.5f;
        } else
        {
            moveSpeedActual = moveSpeed;
        }
        movement(); 
        if (dashColdown > 0) dashColdown -= Time.fixedDeltaTime;
        if (dashColdown < 0) dashColdown = 0;

        animator.SetFloat("Y", transform.position.y);

        if (timerTillKnockdownReset > 0)
        {
            timerTillKnockdownReset -= Time.deltaTime;
            if (timerTillKnockdownReset <= 0)
            {
                endKnockdownTimer();
            }
        }

        if (armorRegenTimeLeft > 0)
        {
            armorRegenTimeLeft -= Time.deltaTime;
            if (armorRegenTimeLeft <= 0)
            {
                armorRegenTimeLeft = 0;
                endArmorRegenTimer();
            }
        }
    }

    void movement()
    {
        if (!blockMovement && (moveX != 0 || moveY != 0))
        {
            moveX *= moveSpeedActual * Time.deltaTime;
            moveY *= moveSpeedActual / 2 * Time.deltaTime;

            Vector3 step = new Vector3(moveX, 0, moveY);

            if (sprint)
            {
                step *= 2;
            }

            if (step.magnitude > 0 && !hoverAround)
            {
                if (moveX < 0)
                {
                    transform.localScale = lookLeft;
                    facing = -1;
                }
                else if (moveX != 0)
                {
                    transform.localScale = lookRight;
                    facing = 1;
                }

                animator.SetBool("IsWalking", true);
                transform.Translate(step, Space.Self);
            } else if (step.magnitude > 0 && hoverAround)
            {
                if (transform.position.x <= target.position.x)
                {
                    transform.localScale = lookRight;
                    facing = 1;
                }
                else 
                {
                    transform.localScale = lookLeft;
                    facing = -1;
                }
                animator.SetBool("IsWalking", true);
                transform.Translate(step, Space.Self);
            }
            else
            {
                animator.SetBool("IsWalking", false);
            }
        } else
        {
            animator.SetBool("IsWalking", false);
        }
    }

    public void flip(int direction = 0)
    {
        if (direction == 0)
        {
            if (transform.localScale == lookLeft)
            {
                transform.localScale = lookRight;
                facing = 1;
            }
            else
            {
                transform.localScale = lookLeft;
                facing = -1;
            }
            return;
        }
        if (direction > 0)
        {
            transform.localScale = lookRight;
            facing = 1;
        } else
        {
            transform.localScale = lookLeft;
            facing = -1;
        }
    }

    public void kick()
    {
        damageCurrent = damageKick;
        blockMovement = true;
        blockAttack = true;
        animator.SetBool("Kick1", true);
    }

    void endKick()
    {
        damageCurrent = 0;
        animator.SetBool("Kick1", false);
        blockMovement = false;
        blockAttack = false;

        if (onEndAttack != null)
        {
            onEndAttack();
        }
    }

    public IEnumerator dashing(int direction)
    {
        if (dashColdown == 0 && !blockMovement)
        {
            gameObject.layer = 12;
            dashColdown = dashColdownLength;
            blockAttack = true;
            animator.SetTrigger("DashTr");
            animator.SetBool("Dash", true);
            for (int i = 0; i < 4; i++)
            {
                transform.Translate(moveSpeedActual * 0.1f * direction, 0, 0);
                yield return null;
            }

            animator.SetBool("Dash", false);

            for (int i = 0; i < 3; i++)
            {
                transform.Translate(moveSpeedActual * 0.04f * direction, 0, 0);
                yield return null;
            }
            if (onEndDash != null)
            {
                onEndDash();
            }
            animator.SetBool("Dash", false);
            blockAttack = false;
            gameObject.layer = defaultLayer;
            //resetPunch();
        }
    }

    public void punch()
    {
        if (!blockAttack)
        {
            damageCurrent = damagePunch;
            animator.SetBool("Punch", true);
            blockMovement = true;
        }
    }

    public void endPunch()
    {
        damageCurrent = 0;
        animator.SetBool("Punch", false);
        blockMovement = false;
    }
    public void startPunch()
    {
        //changing faciong depend on input
        if (moveX != 0) flip((int)moveX);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == hitTag && !dead)
        {
            if (invulnerable)
            { //do some thing to show that we didnt take damage, but could

            }
            else
            { //taking damage
                //Checking if charachter have block
                if (block != null)
                {
                    //Check if block is active
                    if (block.block)
                    {
                        //Check facing
                        //left
                        if (other.transform.parent.position.x < transform.position.x && facing == -1)
                        {
                            animator.SetTrigger("TakingHitOnBlockTr");
                            return;
                        }

                        //right
                        else if (other.transform.parent.position.x > transform.position.x && facing == 1)
                        {
                            animator.SetTrigger("TakingHitOnBlockTr");
                            return;
                        }
                        else
                        {
                            block.block = false;
                        }
                    }
                }
                startArmorRegenTimer();
                if (armor > 0)
                {
                    armor--;
                    return;
                }

                CharacterControl attackingCharachter = other.transform.parent.GetComponent<CharacterControl>();

                //Dealing damage
                //hp -= attackingCharachter.damageCurrent;
                setHp(hp - attackingCharachter.damageCurrent);
                damageTaken += attackingCharachter.damageCurrent;

                //PopUpTextManager.generate(transform.position, Color.red, attackingCharachter.damageCurrent.ToString());

                startKnockdownTimer();
                if (damageTaken >= knockdownTrashholdDamage)
                {
                    iniKnocdown(other.transform);
                }
                else
                {
                    animator.SetTrigger("TakingHitTr");

                    if (GameManager.Hero == this) invulnerable = true;
                }
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag == "PlayerHitBox")
        {
            //Debug.Log("Player push " + collision.relativeVelocity);
            rigidbody.isKinematic = true;
            //collision.collider.GetComponent<Rigidbody>().AddForce(97 * collision.impulse);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        rigidbody.isKinematic = false;
    }

    void iniKnocdown(Transform other)
    {
        invulnerable = true;
        animator.SetTrigger("KnockBackTr");
        Vector3 knockdownVector = new Vector3(0, 100, 0);

        if (other.parent.position.x < transform.position.x)
        {
            knockdownVector.x = 100;
        }
        else
        {
            knockdownVector.x = -100;
        }

        rigbody.AddForce(knockdownVector);
        endKnockdownTimer();

        gameObject.layer = 14;
    }

    void startKnockdownTimer()
    {
        timerTillKnockdownReset = knockdownTrashholdTime;
    }

    void endKnockdownTimer()
    {
        timerTillKnockdownReset = 0;
        damageTaken = 0;
    }

    void startArmorRegenTimer()
    {
        armorRegenTimeLeft = armorRegenTime;
    }

    void endArmorRegenTimer()
    {
        armorRegenTimeLeft = 0;
        if (armor < armorRegenMax)
        {
            armor++;
            if (armor < armorRegenMax)
            {
                startArmorRegenTimer();
            }
        }
    }

    void endTakingHit()
    {
        if (onEndTakingHit != null)
        {
            onEndTakingHit();
        }
        if (GameManager.Hero == this) invulnerable = false;
    }

    void endKnockdown()
    {
        blockAttack = false;
        blockMovement = false;
        invulnerable = false;
        gameObject.layer = defaultLayer;
    }

    void death()
    {
        gameObject.layer = 12;
        dead = true;
        blockAttack = true;
        blockMovement = true;
        animator.SetTrigger("Death");

        if (onDeath != null) onDeath();
    }

    public Vector3 getClosestAproachPoint(Vector3 incoming)
    {
        float distance = Vector3.Distance(incoming, attackPoints[0].cord + transform.position);
        int index = 0;
        for (int i = 1; i < attackPoints.Count; i++)
        {
            float tempDistance = Vector3.Distance(incoming, attackPoints[i].cord + transform.position);
            if (tempDistance < distance)
            {
                distance = tempDistance;
                index = i;
            }
        }
        //Debug.Log(index + " " + (attackPoints[index].cord + transform.position));
        return attackPoints[index].cord + transform.position;
    }
}

public class characterAttackPoint
{
    //public Transform character;
    //public Transform target;
    public Vector3 cord;
    public Transform takenBy;
    public Dictionary<float, Transform> distanceToEnemies;

    public characterAttackPoint (Vector3 cord)
    {
        this.cord = cord;
    }
}