﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpText : MonoBehaviour {

    public Text textObject;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ini(Vector3 position, Color color, string text, Camera camera)
    {
        Vector2 pos = camera.WorldToScreenPoint(position);
        pos.y += 50;
        textObject.rectTransform.position = pos;

        textObject.color = color;
        textObject.text = text;

        StartCoroutine(selfdestruct());
    }

    public IEnumerator selfdestruct()
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
