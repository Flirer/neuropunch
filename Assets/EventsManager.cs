﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsManager : MonoBehaviour {

    public static EventsManager Instance;

    //Objects for events

    public GameObject miu;
    public GameObject npc1;
    public GameObject npc2;
    public GameObject informator;

    public TextAsset InformatorShortDialog;
    public TextAsset InformatorFinalDialog;

    public Arena CyclopArena;
    public TextAsset CyclopFinDialog;
    //OLD OBJECTS
    /*
    public Arena _1cityhallArena;
    public Entrence _1cityhallEntrence;

    public List<AiEnemyControl> _1cityhallCops;

    public Arena _1cityhallArena2;


    public Entrence hiddenEntrence;

    public actionAble homelessGuy;
    public TextAsset textForSavingHomelessGuy;*/

    //Bools for dialogs
    /*public bool homelessGuysSaved = false;
    public bool copToldAboutSecretEntrence = false;
    public bool copsLeftCityHall = false;*/

    public bool CyclopNotCheckedAsInformator = true;

    public bool passBought = false;
    public bool realPassBought = false;
    public bool greenGaveQuest = false;

    public bool informatorRevealed = false;
    public bool GreenGaveInformation = false;

    public Arena ClubArena;

    // Use this for initialization
    void Start () {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        PlayerControl.Instance.controlIsEnabled = false;
        ClubArena.onArenaClear += GameManager.Instance.iniVictory;
        StartCoroutine(waitForStart());
    }

    IEnumerator waitForStart()
    {
        yield return new WaitForSeconds(1);
        miu.GetComponent<DialogLuncher>().lunchDialog();
        PlayerControl.Instance.controlIsEnabled = true;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void LunchEvent(string eventName)
    {
        switch (eventName)
        {
            /*
            case "cityHallCopsLeaveForHomeless":

                foreach (AiEnemyControl ai in Instance._1cityhallCops)
                {
                    ai.GetComponent<actionAble>().arena.enemiesInArena.Remove(ai);
                    ai.GetComponent<actionAble>().arena = null;

                    Vector3 newPoint = ai.transform.position;
                    newPoint.x += 7;

                    ai.changeState(AiEnemyControl.AiState.moveTo, newPoint);
                }
                Instance._1cityhallCops[0].onFinishMoveTo += Instance.finishFirstEvent;
                Instance._1cityhallEntrence.GetComponent<actionAble>().dialog = false;
                Instance.copsLeftCityHall = true;
                break;

            case "cityHallCopsAttachToArena3":

                foreach (AiEnemyControl ai in Instance._1cityhallCops)
                {
                    ai.GetComponent<actionAble>().arena = Instance._1cityhallArena2;
                }
                Instance._1cityhallArena2.rearmArena(Instance._1cityhallCops);

                break;

            case "unlockHiddenEntrence":
                Instance.hiddenEntrence.gameObject.SetActive(true);
                break;

            case "helpHomelessGuy":
                Instance.homelessGuysSaved = true;
                Instance.homelessGuy.dialogLuncher.dialog = Instance.textForSavingHomelessGuy;
                break;
            */
            //old material
            case "CyclopedCheckedAsInformator":
                Instance.CyclopNotCheckedAsInformator = false;
                break;

            case "finishingIniWithMiu":
                Destroy(Instance.miu);
                break;

            case "disablingNpc1":
                Instance.npc1.GetComponent<actionAble>().turnOff();
                break;

            case "disablingNpc2":
                Instance.npc2.GetComponent<actionAble>().turnOff();
                break;

            case "finishingInformator":
                Instance.informator.GetComponent<DialogLuncher>().dialog = Instance.InformatorShortDialog;
                break;

            case "disablingInformator":
                Instance.informator.GetComponent<actionAble>().turnOff();
                
                break;

            case "GreenGiveQuest":
                Instance.greenGaveQuest = true;
                break;

            case "EngageCyclop":
                Instance.npc2.GetComponent<DialogLuncher>().dialog = Instance.CyclopFinDialog;
                break;

            case "GreenQuestRewardMoney":
                GameManager.Instance.nanomachines += 75;
                break;

            case "GreenQuestRewardAttack":
                UpgraidManager.Instance.onAttackUpgraid();
                break;

            case "GreenQuestRewardHealth":
                UpgraidManager.Instance.onHpUpgraid();
                break;

            case "GreenQuestComplete":
                Instance.GreenGaveInformation = true;
                break;

            case "RevealInformator":
                Instance.informatorRevealed = true;
                break;

            case "EngageInformator":
                Instance.informator.GetComponent<DialogLuncher>().dialog = Instance.InformatorFinalDialog;
                break;

            case "getRealPass":
                Instance.realPassBought = true;
                break;

            case "getPass":
                Instance.passBought = true;
                break;

            case "sendCopsToInf":
                for (int i = 2; i <= 4; i++)
                {
                    AiEnemyControl ai = Instance.ClubArena.enemiesInArena[i];
                    Vector3 newPoint = ai.transform.position;
                    newPoint.x += -7;
                    ai.changeState(AiEnemyControl.AiState.moveTo, newPoint);
                    Debug.Log(ai.name);
                }
                Instance.ClubArena.enemiesInArena.RemoveAt(4);
                Instance.ClubArena.enemiesInArena.RemoveAt(3);
                Instance.ClubArena.enemiesInArena.RemoveAt(2);
                break;

            case "victory":
                Debug.Log("VICTORY");
                Instance.ClubArena.isSafe = true;
                GameManager.Instance.iniVictory();
                break;

            default:
                Debug.Log("Event not found");
                break;

        }
    }

    void victory()
    {
        LunchEvent("victory");
    }

    public void finishFirstEvent()
    {
        LunchEvent("cityHallCopsAttachToArena3");
    }
}
