﻿using AdvancedInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[AdvancedInspector]
public class actionAble : MonoBehaviour {

    public enum eActions {dialog, entrence, upgraidsScreen, container};
    [Inspect]
    [Help("HelpAction")]
    public eActions action;

    [Inspect("showDrop")]
    public List<PickUp> drop;

    [Inspect("showDrop")]
    public Transform pointOfDrop;

    //Object that will be highlighted when charachter aproach
    [Inspect(0)]
    [Help("HelpToolTip")]
    [Expandable(Expandable = false)]
    public Transform tooltip;

    [Inspect(0, "showDialogOption")]
    public bool dialog;

    [Inspect(Priority = 1, MethodName = "showArenaOption")]
    public Arena arena;

    [Inspect("showEntrenceOption")]
    public Entrence entrence;


    CharacterControl charachter;
    public DialogLuncher dialogLuncher;
    //**************************
    //**EDITOR STUFF GOES HERE**
    //**************************
    bool showEntrenceOption
    {
        get
        {
            if (action == eActions.entrence) return true;
            else return false;
        }
    }

    bool showDialogOption
    {
        get
        {
            if (action != eActions.upgraidsScreen && action != eActions.dialog) return true;
            else return false;
        }
    }

    bool showArenaOption
    {
        get
        {
            if (action != eActions.upgraidsScreen) return true;
            else return false;
        }
    }

    bool showDrop
    {
        get
        {
            if (action == eActions.container) return true;
            else return false;
        }
    }

    private HelpItem HelpAction()
    {
        if (action == eActions.dialog)
        {
            DialogLuncher dialogLuncher = GetComponent<DialogLuncher>();
            if (dialogLuncher == null)
            {
                return new HelpItem(HelpType.Error, "Provide Dialog Luncher Component");
            }
            else
            {
                return null;
            }
        }
            
        else if (action == eActions.entrence)
        {
            if (entrence == null)
            {
                return new HelpItem(HelpType.Error, "Provide Entrence");
            }
            else
            {
                return null;
            }
        }
        else if (action == eActions.container)
        {
            if (drop.Count < 1)
            {
                return new HelpItem(HelpType.Warning, "Provide Drop");
            }
            else
            {
                return null;
            }
        }
        else if (action == eActions.upgraidsScreen)
        {
            return null;
        }

        else
            return new HelpItem(HelpType.Error, "Set action");
    }

    private HelpItem HelpToolTip()
    {
        if (tooltip == null)
        {
            return new HelpItem(HelpType.Error, "Provide Tooltip");
        } else
        {
            return new HelpItem(HelpType.Info, "ToolTip object, explain to player what kind of action will be preformed.");
        }
    }

    /*private HelpItem HelpDialog()
    {
        if (action == eActions.dialog)
        {
            if (dialog == null)
            return new HelpItem(HelpType.Error, "Provide Dialog TextAsset.");
            else
            return new HelpItem(HelpType.Info, "Player will lunch this dialog on Activation.");
        }
        else if (action == eActions.entrence)
        {
            if (dialog == null)
                return new HelpItem(HelpType.Info, "If provided, will lunch instead of using entrence directly.");
            else
                return new HelpItem(HelpType.Info, "Dialog will be lunched instead of Activation.");
        } else
        {
            return null;
        }
    }

    private HelpItem HelpArena()
    {
        if (action == eActions.dialog)
        {
            return new HelpItem(HelpType.Info, "If Dialog can lunch Generic Combat, same dialog used for many charachters, you cant specify witch Arena to engage in Dialog, set related Arena here.");
        }
        else if (action == eActions.entrence)
        {
            if (dialog == null)
                return new HelpItem(HelpType.Info, "Arena will be engaged on Activating this Entrence. After clearing Arena player can use Entrence as usual.");
            else
                return new HelpItem(HelpType.Info, "Provided Arena will be related to Dialog.");
        }
        else
        {
            return null;
        }
    }

    private HelpItem HelpEntrence()
    {
        if (dialog == null)
            return new HelpItem(HelpType.Error, "Provide Entrence, where charachter will be transfered.");
        else
            return new HelpItem(HelpType.Info, "Location, where player will be transfered on activation.");
    }*/

    //***********************
    //**END OF EDITOR STUFF**
    //***********************

    bool showingToolTip = false;
	// Use this for initialization
	void Start () {
        charachter = GetComponent<CharacterControl>();
        dialogLuncher = GetComponent<DialogLuncher>();
        if (charachter)
        {
            charachter.onDeath += turnOff;
        }

        tooltip.Find("BG").Find("Text").GetComponent<Text>().text += "[E]";
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.actionableItem == this)
        {
            if (showingToolTip) return;
            else showToolTip();
        } else
        {
            if (!showingToolTip) return;
            else hideToolTip();
        }
	}

    public void turnOff()
    {
        hideToolTip();
        if (GameManager.actionableItem == this) GameManager.actionableItem = null;

        if (actionSearcher.Instance.itemsAround.Contains(this)) actionSearcher.Instance.itemsAround.Remove(this);

        enabled = false;
    }

    public void showToolTip()
    {
        tooltip.gameObject.SetActive(true);
        showingToolTip = true;
    }

    public void hideToolTip()
    {
        tooltip.gameObject.SetActive(false);
        showingToolTip = false;
    }

    public void engageInAction()
    {
        switch (action)
        {
            case eActions.dialog:

                /*if (arena != null) DialogueManager.currentlyRelatedArena = arena;
                DialogueManager.Instance.StartDialogue(dialog);*/
                dialogLuncher.lunchDialog();
                break;

            case eActions.entrence:

                if (dialog)
                {
                    /*if (arena != null) DialogueManager.currentlyRelatedArena = arena;
                    DialogueManager.Instance.StartDialogue(dialog);*/
                    dialogLuncher.lunchDialog();
                } else if (arena != null && !arena.cleared)
                {
                    if (arena.dialog) arena.dialogLuncher.lunchDialog();
                    else arena.engageArena();
                } else
                {
                    GetComponent<Entrence>().use();
                }

                break;

            case eActions.upgraidsScreen:

                UpgraidManager.showScreen();

                break;

            case eActions.container:
                

                foreach (PickUp PickUp in drop)
                {
                    Instantiate(PickUp.gameObject, pointOfDrop.position, Quaternion.identity);
                    Debug.Log("Drop stuff " + PickUp.name);
                }
                turnOff();
                hideToolTip();

                break;
        }
    }
}
